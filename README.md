# FortuneCookieExemple
Cette application est un exemple pour la cr�ation d'un interface de type JavaFx sur une programme existant.
Ce paquet contient tout pour pouvoir commencer � programmer.

# Installation
Pour la premi�re partie, il faut installer le module e(fx)clipse de Eclipse.
Par la suite, il reste � installer le Scene Buider. L'executable est dans le repertoire doc.

N'oublier pas -> mvn clear install

Durant l'installation de Scene Builder il faut prendre en note du repertoire du scene buider 
Exemple: C:\Users\Portable\AppData\Local\SceneBuilder

# Configuration de JavaFX et Scene Builder
Apr�s que tous soient installer, il faut fait une configuration pour faciliter la programmation.
Aller windows -> preference -> JavaFX

Mettez le chemin de l'executable Scene Buider.

# Exercise
Maintenant, le code est trou�. Il faut remplir le code pour mettre les fonctionnalit�s. Le code sera comment� ou suprim� et on va ajouter les fonctionnalit� une par une.

# Solution
La solution est dans la branche Solution.
