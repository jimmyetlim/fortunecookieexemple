package com.seminar;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class FortuneCookie extends Application{
	
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		 Parent loader = FXMLLoader.load(getClass().getResource("vue/FortuneCookieInterface.fxml"));
		 Scene scene  = new Scene(loader, 300, 500);
		 primaryStage.setResizable(false);
		 primaryStage.setScene(scene);
		 primaryStage.setTitle("Fortune Cookie");
		 primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("vue/img/Emoticons_icon.png")));
		 primaryStage.show();
		 
	}

}
