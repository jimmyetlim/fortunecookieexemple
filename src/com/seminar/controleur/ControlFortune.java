package com.seminar.controleur;

import java.util.ArrayList;
import java.util.List;
import com.seminar.model.FortuneTread;
import com.seminar.model.Observable;
import com.seminar.model.Observer;
import com.seminar.model.TestThread;

public class ControlFortune implements Observable{

	private List<Observer> observers;
	
	public ControlFortune() {
		super();
		this.observers = new ArrayList<>();
	}

	@Override
	public void setObserver(Observer o) {
		observers.add(o);
		
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
		
	}

	@Override
	public synchronized void notifyObserver(String text, int cat) {
		for (Observer observer : observers) {
			observer.update(text, cat);
		}
	}
	
	public Thread getFortune(String type, String name){
		FortuneTread runnable = new FortuneTread(type, name, this);
		return new Thread(runnable);
		
	}
	
	public void getFortune( String name, List<String> list){
		List<Thread> resultat = new ArrayList<>() ;
		for (String type : list) {
			resultat.add( getFortune( type,  name));
		}
		for (Thread thread : resultat) {
			thread.start();
		}
	}
	
	public void getFortuneTest(){
		int number= 100;
		List<Thread> resultat = new ArrayList<>() ;
		for (int i = 0; i < number; i++) {
			resultat.add( new Thread( new TestThread(i, this)));
		}
		for (Thread thread : resultat) {
			thread.start();
		}
		
	}

	@Override
	public void actionDispatch(String mode, String nom, List<String> horoscope) {
		if(mode.equals("Horoscope")) {
			 getFortune( nom,horoscope);
		}else if(mode.equals("Test")) {
			getFortuneTest();
		}else {
			getFortune(mode, nom).start();
		}
		
	}
	
}
