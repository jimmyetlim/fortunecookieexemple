package com.seminar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Portable
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fortune {
	private int id;
	private String type;
	private String message;
	private String conseilPos;
	private String conseilNeg;
	private int stat;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getConseilPos() {
		return conseilPos;
	}
	public void setConseilPos(String conseilPos) {
		this.conseilPos = conseilPos;
	}
	public String getConseilNeg() {
		return conseilNeg;
	}
	public void setConseilNeg(String conseilNeg) {
		this.conseilNeg = conseilNeg;
	}
	public int getStat() {
		return stat;
	}
	public void setStat(int stat) {
		this.stat = stat;
	}
	@Override
	public String toString() {
		return "$$$$$ -> "+type+" <- $$$$$\n  [Fortune] : "+message+"\n  [+] : "+conseilPos+"\n  [-] : "+conseilNeg+"\n";
	}

	
	
	
}
