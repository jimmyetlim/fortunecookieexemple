package com.seminar.model;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Random;

import com.fasterxml.jackson.databind.ObjectMapper;

public class FortuneTread implements Runnable{
	private String type;
	private String name;
	private Observable control;

	public FortuneTread(String type, String name, Observable control) {
		super();
		this.type = type;
		this.name = name;
		this.control = control;
	}

	@Override
	public void run() {
		Calendar cal = Calendar.getInstance();
		int number = (cal.get(Calendar.DAY_OF_MONTH) * cal.get(Calendar.MONTH) +nameToNumber(name)+cal.get(Calendar.YEAR)) ;
		Random generator = new Random(number);
		number =Math.abs(generator.nextInt()%10);
		File file = new File("data/fortune/"+type+"/"+number+".json");
		try {
			Fortune result = new ObjectMapper().readValue(file, Fortune.class);
			control.notifyObserver(result.toString(), result.getStat());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public int nameToNumber(String name) {
		int number = 0 ; 
		for (int i = 0; i < name.length(); i++) {
			number += name.charAt(i);
		}
		return number;
	}

}
