package com.seminar.model;

import java.util.List;

public interface Observable {
	
	public void setObserver(Observer o);
	public void removeObserver(Observer o);
	public void  notifyObserver(String text, int cat);
	public void actionDispatch(String mode, String nom, List<String> horoscope);
	
}
