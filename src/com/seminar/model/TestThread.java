package com.seminar.model;

public class TestThread implements Runnable{
	private int id;
	private Observable control;

	public TestThread(int id ,Observable control) {
		super();
		this.id = id;
		this.control = control;
	}

	@Override
	public void run() {
		control.notifyObserver("This is ID #"+id+"\n", 2);
		
	}

}
