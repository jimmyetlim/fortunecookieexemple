package com.seminar.vue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import com.seminar.controleur.ControlFortune;
import com.seminar.model.Observable;
import com.seminar.model.Observer;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class FortureCookieControleur implements Initializable, Observer{
	@FXML
	private AnchorPane window;
	@FXML
	private TextArea message;
	@FXML
	private ComboBox<String> type;
	@FXML
	private ListView<CheckBox> list;
	@FXML
	private TextField nom;
	
	private Observable mainframe; 
	private Boolean resp;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		//set observer
		mainframe = new ControlFortune();
		mainframe.setObserver(this);
		
		
		type.setItems(getListType());
		setListCheckBox();
		
		
		type.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				setListCheckBox();
			}
		});
	}
	
	public void setListCheckBox() {
		list.setItems(getListAttribut());
		list.setDisable(list.getItems().isEmpty());
	}
	
	public void doAction() throws IOException {
		
		writeInConsole("Hello World");
		
		String nameValue = extractValueTextField(nom);
		String typeValue = extractText(type).replaceAll(" ", "_");;
		if(nameValue!=null && typeValue!=null && !nameValue.equals("") && !typeValue.equals("")) {
			final Stage dialog = new Stage();
			resp = false;
			FXMLLoader loader = new FXMLLoader(getClass().getResource("PopUp.fxml"));
			Parent root = loader.load();
			 PopUpControleur popControleur = loader.getController();
			 popControleur.setUserData(nameValue, typeValue,dialog, this);
			 popControleur.reloadData();
			
			 Scene scene  = new Scene(root, 300, 150);
			 dialog.setResizable(false);
			 dialog.setScene(scene);
			 dialog.setTitle("Confirmation");
			 dialog.showAndWait();
			if(resp) {
				erase();
				List<String> horoscope = extractList(list);
				mainframe.actionDispatch(typeValue, nameValue, horoscope);
			}
		}else {
			erase();
			writeInConsole("Valeur non Valide");
		}
	}
	
	private List<String> extractList(ListView<CheckBox> list2) {
		List<String> listValue = new ArrayList<>();
		if(list2.isDisable())
			return null;
		ObservableList<CheckBox>listBox = list2.getItems();
		for (CheckBox checkBox : listBox) {
			if(checkBox.isSelected()) {
				listValue.add(checkBox.getText().replace(' ', '_'));
			}
		}
		return listValue;
	}

	public void writeInConsole(String text) {
		message.appendText(text);
	}
	
	public void writeFortune(String text,int cat) {
		writeInConsole(text);
		if(cat==1) {
			window.getStyleClass().remove("neutral");
			window.getStyleClass().remove("bad");
			window.getStyleClass().add("good");
		}else if(cat==0){
			window.getStyleClass().remove("good");
			window.getStyleClass().remove("bad");
			window.getStyleClass().add("neutral");
		}else{
			window.getStyleClass().remove("neutral");
			window.getStyleClass().remove("good");
			window.getStyleClass().add("bad");	
		}
	}
	
	public ObservableList<String> getListType() {
		List<String> typeList = new ArrayList<String>();
		typeList.addAll(Arrays.asList(getListTag()));
		typeList.add("Horoscope");
		typeList.add("Test");
		return FXCollections.observableArrayList(typeList);
	}
	
	public ObservableList<CheckBox> getListAttribut(){
		List<CheckBox> attList = new ArrayList<>();
		String mode = extractText(type);
		if(mode.equals("Horoscope")) {
			String[] tags = getListTag();
			attList.addAll(toCheckBox(tags));
		}
		
		return FXCollections.observableArrayList(attList);
		
	}
	
	public List<CheckBox> toCheckBox(String[] tags){
		List<CheckBox> listCheckBox = new ArrayList<>();
		for (String names : tags) {
			CheckBox check = new CheckBox();
			check.setText(names);
			check.setSelected(true);
			listCheckBox.add(check);
		}
		return listCheckBox;
	}
	
	public String[] getListTag() {
		File file = new File("data/fortune");
		String[] dirs = file.list();
		for (int i = 0 ; i < dirs.length ; i++) {
			dirs[i] = dirs[i].replace('_', ' ');
		}
		return dirs;
	}
	
	private String extractValueTextField(TextField text) {
		return text.getText();
	}
	
	private String extractText(ComboBox<String> type2) {
		String text = type2.getValue();
		return (text ==null)?"":text;
	}
	public void response(Boolean resp) {
		this.resp = resp;
	}

	public void erase() {
		message.clear();
		window.getStyleClass().remove("bad");
		window.getStyleClass().remove("good");
		window.getStyleClass().add("neutral");
	}

	@Override
	public void update(String text, int cat) {
		writeFortune( text, cat);
	}
	
}
