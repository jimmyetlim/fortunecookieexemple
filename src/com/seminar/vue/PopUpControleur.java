package com.seminar.vue;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class PopUpControleur implements Initializable {

	private String nom;
	private String type;
	private FortureCookieControleur demandeur;
	private Stage stage;
	
	@FXML
	private	Text text; 
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		text.setText("Est-ce que vous �tes sur de vouloir chercher le futur de "+nom+" sur "+type+"?");
	}
	public void reloadData(){
		text.setText("Est-ce que vous �tes sur de vouloir chercher le futur de "+nom+" sur "+type+"?");
	}
	public void setUserData(String nom, String type,Stage stage,FortureCookieControleur demandeur) {
		this.nom = nom;
		this.type = type;
		this.demandeur = demandeur;
		this.stage = stage;
	}
	
	public void responsePositif(){
		demandeur.response(new Boolean(true));
		close();
	}
	
	public void responseNegatif(){
		demandeur.response(new Boolean(false));
		close();
	}
	public void close(){
		stage.close();
	}

}
